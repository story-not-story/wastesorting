package school.wastesorting.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import school.wastesorting.domain.Luckymoney;
import school.wastesorting.enums.ErrorCode;
import school.wastesorting.exception.LuckymoneyException;
import school.wastesorting.repository.LuckymoneyRepository;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public class LuckymoneyService {
    @Autowired
    private LuckymoneyRepository luckymoneyRepository;

    @Transactional
    public void createTwo(){
        Luckymoney l1 = new Luckymoney();
        l1.setComsumer("hujun");
        l1.setProducer("taotao");
        l1.setMoney(BigDecimal.valueOf(198.2));
        luckymoneyRepository.save(l1);
        Luckymoney l2 = new Luckymoney();
        l2.setComsumer("taotao");
        l2.setProducer("hujun");
        l2.setMoney(BigDecimal.valueOf(1008.2));
        luckymoneyRepository.save(l2);
    }

    public String getMoney(Integer id) throws Exception {
        Optional<Luckymoney> optional = luckymoneyRepository.findById(id);
        if (optional.isPresent()){
            BigDecimal money = optional.get().getMoney();
            if (money.compareTo(BigDecimal.valueOf(100)) <= 0) {
                throw new LuckymoneyException(ErrorCode.LITTLE_MONEY);
            } else if (money.compareTo(BigDecimal.valueOf(1000)) <= 0) {
                throw new LuckymoneyException(ErrorCode.MUCH_MONEY);
            } else {
                return "very much money";
            }
        }
        return "unknown money";
    }
}
