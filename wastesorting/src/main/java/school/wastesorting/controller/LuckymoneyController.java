package school.wastesorting.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import school.wastesorting.enums.ErrorCode;
import school.wastesorting.domain.Luckymoney;
import school.wastesorting.domain.Result;
import school.wastesorting.repository.LuckymoneyRepository;
import school.wastesorting.service.LuckymoneyService;
import school.wastesorting.util.ResultUtil;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class LuckymoneyController {
    @Autowired
    private LuckymoneyRepository luckymoneyRepository;

    @Autowired
    private LuckymoneyService luckymoneyService;

    private static final Logger logger = LoggerFactory.getLogger(LuckymoneyController.class);

    @RequestMapping(value = "/luckymoney", method = RequestMethod.GET)
    public Result<List<Luckymoney>> select(@RequestParam(value = "id", required = false) Integer id){
        logger.info("select");
        if (id != null) {
            Optional<Luckymoney> optional = luckymoneyRepository.findById(id);
            if (optional.isPresent()) {
                List<Luckymoney> list = new ArrayList<Luckymoney>();
                list.add(optional.get());
                return ResultUtil.success(list);
            } else {
                return ResultUtil.fail(ErrorCode.LUCKMONEY_NOT_FOUND);
            }
        }
        return ResultUtil.success(luckymoneyRepository.findAll());
    }


    @RequestMapping(value = "/luckymoney", method = RequestMethod.POST)
    public Result<Luckymoney> create(@Valid  Luckymoney luckymoney, BindingResult bindingResult){
        logger.info("create");
        if (bindingResult.hasErrors()) {
            return ResultUtil.fail(ErrorCode.PARAM_ERROR);
        }
        luckymoney.setMoney(luckymoney.getMoney());
        luckymoney.setProducer(luckymoney.getProducer());
        luckymoney.setComsumer(luckymoney.getComsumer());
        return ResultUtil.success(luckymoneyRepository.save(luckymoney));
    }

    @RequestMapping(value = "/luckymoney", method = RequestMethod.PUT)
    public Result<Luckymoney> update(@RequestParam(value = "comsumer") String comsumer,
                     @RequestParam(value = "id") Integer id){
        logger.info("update");
        Optional<Luckymoney> result = luckymoneyRepository.findById(id);
        if (result.isPresent()) {
            result.get().setComsumer(comsumer);
            return ResultUtil.success(luckymoneyRepository.save(result.get()));
        }
        return ResultUtil.fail(ErrorCode.LUCKMONEY_NOT_FOUND);
    }

    @RequestMapping(value = "/luckymoney", method = RequestMethod.DELETE)
    public Result<Luckymoney> delete(@RequestParam(value = "id") Integer id){
        logger.info("delete");
        Optional<Luckymoney> result = luckymoneyRepository.findById(id);
        if (result.isPresent()) {
            luckymoneyRepository.deleteById(id);
            return ResultUtil.success();
        }
        return ResultUtil.fail(ErrorCode.LUCKMONEY_NOT_FOUND);
    }

    @RequestMapping(value = "/money", method = RequestMethod.GET)
    public Result<String> getMoney(@RequestParam(value = "id") Integer id) throws Exception {
        logger.info("getMoney");
        return ResultUtil.success(luckymoneyService.getMoney(id));
    }
}
