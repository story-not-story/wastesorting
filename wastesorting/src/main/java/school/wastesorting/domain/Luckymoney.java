package school.wastesorting.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import java.math.BigDecimal;

@Entity
public class Luckymoney {
    @Id
    @GeneratedValue
    private Integer id;
    @Min(value = 1, message = "最少1")
    private BigDecimal money;
    private String producer;
    private String comsumer;

    public Luckymoney() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    /**
     * 发送方
     * @return
     */
    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getComsumer() {
        return comsumer;
    }

    public void setComsumer(String comsumer) {
        this.comsumer = comsumer;
    }

    public String toString(){
        return "Producer=" + producer + ";Comsumer=" + comsumer + ";Money=" + money;
    }
}
