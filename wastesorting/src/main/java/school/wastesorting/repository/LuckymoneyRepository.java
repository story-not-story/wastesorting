package school.wastesorting.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import school.wastesorting.domain.Luckymoney;

public interface LuckymoneyRepository extends JpaRepository<Luckymoney, Integer> {
}
