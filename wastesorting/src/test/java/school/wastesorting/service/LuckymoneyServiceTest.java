package school.wastesorting.service;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import school.wastesorting.enums.ErrorCode;
import school.wastesorting.exception.LuckymoneyException;

import static org.junit.jupiter.api.Assertions.*;
@RunWith(SpringRunner.class)
@SpringBootTest
class LuckymoneyServiceTest {
    @Autowired
    private LuckymoneyService luckymoneyService;

    @Test
    void createTwo() {
    }

    @Test
    void getMoney() {
        try {
            String msg = luckymoneyService.getMoney(15);
            Assert.assertEquals(msg, "very much money");
            String msg2 = luckymoneyService.getMoney(16);
        } catch (LuckymoneyException e) {
            Assert.assertEquals(e.getMessage(), ErrorCode.LITTLE_MONEY.getMessage());
            Assert.assertEquals(e.getCode(), ErrorCode.LITTLE_MONEY.getCode());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}